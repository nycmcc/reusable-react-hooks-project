import React from 'react';
import useResources from './useResources';

// don't need to pass a prop because want to get a list of users 100% of the time
const UserList = () => { 
  const users = useResources('users');

  return (
    <ul>
      {users.map(user => <li key={user.id}>{user.name}</li>)}
    </ul>
  )
} 

export default UserList