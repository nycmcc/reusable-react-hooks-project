// I'm using a lowercase here because by convention we usually call function or some files
// that export a function with a lowercase whereas Riak components always get capitalized.

import {useState, useEffect} from 'react';
import axios from 'axios'

const useResources = (resource) => { // resource = prop
  const [resources, setResources] = useState([]); // this.state = [];
  useEffect(
    () => {
      (async resource => {
        const response = await axios.get(
          `https://jsonplaceholder.typicode.com/${resource}`
        );

        setResources(response.data);
      })(resource); // immediately invoke with resource
    }, 
    [resource]
  );

  return resources;
};

export default useResources;