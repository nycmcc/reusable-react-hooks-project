import React from 'react';
import axios from 'axios';

class ResourceList extends React.Component {
  state = { resources: [] };

  // loading data => request to get the lists of posts or todos
  // gets invoked single time
  async componentDidMount() {
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/${this.props.resource}`
    );

    this.setState({ resources: response.data })
  }

  // componentDidupdate() => this lifecycle method gets called any time that our 
  // component renders either because of a parent component or any time that we 
  // call setStates inside of this class based component.
  async componentDidUpdate(prevProps) {
    // console.log(prevProps);
    if(prevProps.resource !== this.props.resource){
      const response = await axios.get(
      `https://jsonplaceholder.typicode.com/${this.props.resource}`
    );

    this.setState({ resources: response.data })
    }
  }

  render() {
    return <div>{this.state.resources.length}</div>;
  }
}

export default ResourceList;