import React, {useState, useEffect} from 'react';
import axios from 'axios';

// Perfect example of closure

// asynchronous logic
const ResourceList = ({ resource }) => {
  const [resources, setResources] = useState([]); // this.state = [];
  const fetchResource = async (resource) => {
    const response = await axios.get(
      `https://jsonplaceholder.typicode.com/${resource}`
    );

    setResources(response.data);
  }

  // useEffect()
  // Remember it's all about what value you are passing into that array
  // 3 ways to use useEffect
  //   1) if you want the function right there to be ran every single time your component 
  //      is rendered:
  //      
  //          useEffect(
  //            () => {
  //              fetchResource(resource);
  //            }
  //          );

  //   2) called one time => if you want to make sure that that arrow function only gets 
  //      called one time. Put in an empty array:
  //            useEffect(
  //            () => {
  //              fetchResource(resource);
  //            },
  //            []
  //          );
  //   3) And if you want to institute any type of check to limit how often it's called 
  //      just pass in some value:
  //          useEffect(
  //            () => {
  //              fetchResource(resource);
  //            },
  //            [resource]
  //          );
  // cannot use useEffect if we are passing in a async function or a function that 
  // returns a promise.

  // componentDidMount(this is a closure)
  useEffect(
    () => {
      fetchResource(resource);
    }, 
    // componentDidUpdate
    [resource]
  ); 

  // Alternatively a different way of handling this approach, can actually define this 
  // arrow function inside of that as a function and then invoke it immediately.

  const ResourceList = ({ resource }) => {
  const [resources, setResources] = useState([]); // this.state = [];
  
  useEffect(
    () => {
      (async resource => {
        const response = await axios.get(
          `https://jsonplaceholder.typicode.com/${resource}`
        );

        setResources(response.data);
      })(resource); // immediately invoke with resource
    }, 
    [resource]
  ); 

  }

  return (
    <div>
      {resources.length}
  </div>);
}

export default ResourceList