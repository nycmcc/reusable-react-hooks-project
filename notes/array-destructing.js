const colors = ['red', 'green'];

const myColorOne = colors[0];
const myColorTwo = colors[1];

console.log(myColorOne); // red
console.log(myColorTwo) // green

// right here is destructing an array
const [myColorThree, myColorFour] = colors; // myColorThree = red and myColorFour = green